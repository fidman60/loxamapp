import React from 'react';
import {ScrollView, View} from "react-native";
import BookingHeader from "../headers/BookingHeader";
import BookingSearchInput from "../search-inputs/BookingSearchInput";
import LoadingIndicator from "../loading/LoadingIndicator";

export default class BookingLayout extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            isLoading: true,
        }
    }

    componentDidMount(): void {
        setTimeout(() => {
            this.setState({
                isLoading: false,
            });
        },  10);
    }


    render() {
        const {isLoading} = this.state;

        return (
            <View style={styles.main_container}>
                {isLoading && <LoadingIndicator/>}
                {!isLoading && <React.Fragment>
                    <BookingHeader/>
                    <ScrollView>
                        <View style={styles.scrollView}>
                            <View style={styles.searchInput}>
                                <BookingSearchInput/>
                            </View>
                            {this.props.children}
                        </View>
                    </ScrollView>
                </React.Fragment>}
            </View>
        );
    }

}

const styles = {
    main_container: {
        flex: 1,
    },
    scrollView: {
        flex: 1,
        marginTop: 20,
        paddingTop: 20,
        backgroundColor: "#e7eff8",
        paddingLeft: 10,
        paddingRight: 10,
        borderTopRightRadius: 40,
        borderTopLeftRadius: 40,
        paddingBottom: 20
    },
    searchInput: {
        paddingRight: 20,
        paddingLeft: 20,
    }
};