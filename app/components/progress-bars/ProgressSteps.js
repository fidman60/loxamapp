import React from 'react';
import {StyleSheet, Text, View} from "react-native";
import Icon from "react-native-vector-icons/MaterialIcons";
import PropTypes from 'prop-types';

export default class ProgressSteps extends React.PureComponent {

    render() {

        const {stepNo} = this.props;

        const barWidth = stepNo === 1 ? "25%" : stepNo === 2 ? "75%":"100%";

        return (
            <View style={styles.container}>
                <View style={styles.placeHolderBar}/>
                <View style={[styles.barProgress, {width: barWidth}]}/>
                <View style={styles.step}>
                    {stepNo > 1 ?
                        <Icon name="check" color="white"/>
                        :
                        <Text style={[styles.stepText, {color: "white"}]}>1</Text>}
                </View>
                <View style={[styles.step, {backgroundColor: stepNo > 1 ? 'red':'#d7e1f7'}]}>
                    {stepNo > 2 ?
                        <Icon name="check" color="white"/>
                        :
                        <Text style={[styles.stepText, {color: stepNo < 2 ? '#7d81bd' : "white"}]}>2</Text>}
                </View>
                <View style={[styles.step, {backgroundColor: stepNo > 2 ? 'red':'#d7e1f7'}]}>
                    {stepNo === 3 ?
                        <Icon name="check" color="white"/>
                        :
                        <Text style={[styles.stepText, {color: stepNo < 3 ? '#7d81bd' : "white"}]}>3</Text>}
                </View>
            </View>
        );
    }

}

ProgressSteps.propTypes = {
    stepNo: PropTypes.oneOf([1, 2, 3]).isRequired,
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        height: 33,
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center"
    },
    step: {
        width: 33,
        height: 33,
        borderRadius: 50,
        backgroundColor: "#FB263B",
        justifyContent: "center",
        alignItems: "center"
    },
    stepText: {
        fontSize: 15
    },
    barProgress: {
        position: "absolute",
        height: 6,
        backgroundColor: "#FB263B",
        borderRadius: 3
    },
    placeHolderBar: {
        position: "absolute",
        height: 6,
        width: "100%",
        backgroundColor: "#d7e1f7",
        borderRadius: 3
    }
});