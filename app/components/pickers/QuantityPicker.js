import React from "react";
import {View, TouchableOpacity, Text, StyleSheet} from "react-native";
import Icon from "react-native-vector-icons/MaterialIcons";
import PropTypes from 'prop-types';

export default class QuantityPicker extends React.PureComponent {

    render() {
        const {quantity, onPlus, onMinus} = this.props;

        return (
            <View>
                <View style={styles.container}>
                    <TouchableOpacity style={styles.iconView} onPress={onPlus}>
                        <Icon name="add" color="white" size={15}/>
                    </TouchableOpacity>
                    <View style={styles.quantityItem}>
                        <Text>{quantity}</Text>
                    </View>
                    <TouchableOpacity style={styles.iconView} onPress={onMinus}>
                        <Icon name="remove" color="white" size={15}/>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }

}

PropTypes.propTypes = {
    quantity: PropTypes.number.isRequired,
    onPlus: PropTypes.func.isRequired,
    onMinus: PropTypes.func.isRequired,
};

const styles = StyleSheet.create({
    container: {
        width: 35,
        backgroundColor: "#FB263B",
        flex: 1,
        borderRadius: 10
    },
    iconView: {
        flex:1,
        justifyContent: "center",
        alignItems: "center"
    },
    quantityItem: {
        flex:1,
        backgroundColor: "white",
        justifyContent: "center",
        alignItems: "center"
    }
});