import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
import Icon from "react-native-vector-icons/MaterialIcons";
import PropTypes from 'prop-types';

export default class IconWithBadge extends React.Component {
    render() {
        const { name, badgeCount, color, size, badgeColor } = this.props;
        return (
            <View style={styles.container}>
                <Icon style={{position: "relative", zIndex: 1}} name={name} size={size} color={color} />
                <View
                    style={styles.badge}
                >
                    <View style={[styles.badgeChild, {backgroundColor: badgeColor}]}>
                        <Text style={styles.badgeFont}>
                            {badgeCount}
                        </Text>
                    </View>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        width: 30,
        height: 30,
    },
    badge: {
        position: 'absolute',
        right: 4,
        top: 2,
        borderRadius: 6,
        width: 12,
        height: 12,
        zIndex: 100
    },
    badgeChild: {
        width: 12,
        height: 12,
        borderRadius: 6,
        justifyContent: 'center',
        alignItems: 'center'
    },
    badgeFont: {
        color: 'white',
        fontSize: 8,
        fontWeight: 'bold',
    }
});

IconWithBadge.propTypes = {
    name: PropTypes.string.isRequired,
    badgeCount: PropTypes.number.isRequired,
    color: PropTypes.string.isRequired,
    badgeColor: PropTypes.string.isRequired,
    size: PropTypes.number.isRequired,
};