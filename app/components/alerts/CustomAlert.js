import React from 'react';
import {StyleSheet, Text, View} from "react-native";
import PropTypes from 'prop-types';

export default class CustomAlert extends React.Component {

    render() {
        const {type, text} = this.props;
        let color;

        if (type === 'danger')  color = "#cd315d";
        else if(type === 'info') color = "#4d8bd8";
        else color = "#b68929";

        return (
            <View style={[styles.main, {backgroundColor: color}]}>
                <Text style={styles.text}>{text}</Text>
            </View>
        );
    }

}

CustomAlert.propTypes = {
    type: PropTypes.oneOf(['danger', 'success', 'info']).isRequired,
    text: PropTypes.string.isRequired
};

const styles = StyleSheet.create({
    main: {
        flex: 1,
        padding: 10,
        width: "100%"
    },
    text: {
        color: "white"
    }
});