import React from 'react';
import {View, StyleSheet, Text, Image} from "react-native";
import QuantityPicker from "../pickers/QuantityPicker";
import OrderItem from "./OrderItem";
import PropTypes from 'prop-types';

export default class OrderQuantityItem extends React.Component {

    render() {
        const {quantity, onPlus, onMinus, item} = this.props;
        return (
            <View style={styles.main_container}>
                <OrderItem item={item}/>
                <QuantityPicker
                    quantity={quantity}
                    onPlus={onPlus}
                    onMinus={onMinus}
                />
            </View>
        );
    }

}

OrderQuantityItem.propTypes = {
    quantity: PropTypes.number.isRequired,
    onPlus: PropTypes.func.isRequired,
    onMinus: PropTypes.func.isRequired,
    item: PropTypes.object.isRequired,
};

const styles = StyleSheet.create({
    main_container: {
        flexDirection: "row"
    },
});