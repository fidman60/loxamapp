import React from 'react';
import {View, StyleSheet, Image, Text} from "react-native";
import PropTypes from 'prop-types';

export default class OrderItem extends React.Component {

    shouldComponentUpdate(nextProps: Readonly<P>, nextState: Readonly<S>, nextContext: any): boolean {
        return false;
    }

    render() {
        const {item} = this.props;
        return (
            <View style={styles.container}>
                <Image source={require('../../assets/img/machine.jpg')} style={styles.imageItem} resizeMode="contain"/>
                <View>
                    <Text style={styles.itemName}>{item.title}</Text>
                    <Text style={styles.itemDesc}>{item.description}   <Text style={styles.itemRef}>{item.ref}</Text></Text>
                    <Text style={styles.itemPrice}>{item.price} €</Text>
                </View>
            </View>
        );
    }

}

OrderItem.propTypes = {
    item: PropTypes.object.isRequired
};

const styles = StyleSheet.create({
    container: {
        flexDirection: "row",
        alignItems: "center",
        padding: 10,
        backgroundColor: "white",
        borderRadius: 15,
        flex: 1,
        marginRight: 5
    },
    imageItem: {
        width: 60,
        marginRight: 10
    },
    itemName: {
        color: "#484C7F",
        fontSize: 14
    },
    itemDesc: {
        color: "#FF0032",
        fontSize: 14
    },
    itemRef: {
        color: "#c0c1d3"
    },
    itemPrice: {
        fontSize: 27,
        color: "#FC3852"
    }
});