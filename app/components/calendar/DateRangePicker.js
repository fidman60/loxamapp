import React from 'react'
import {Calendar} from 'react-native-calendars'
import Arrow from "./Arrow";
import moment from 'moment';

const XDate = require('xdate');

type Props = {
    initialRange: React.PropTypes.array.isRequired,
    onSuccess: React.PropTypes.func.isRequired,
};

export default class DateRangePicker extends React.PureComponent {

    state = {isFromDatePicked: false, isToDatePicked: false, markedDates: {}};

    componentDidMount() {
        if (this.props.initialRange) this.setupInitialRange()
    }

    onDayPress = (day) => {
        const toDate = moment(day.dateString).add(6, 'days').format('YYYY-MM-DD');
        let markedDates = {[day.dateString]: {startingDay: true, color: this.props.theme.markColor, textColor: this.props.theme.markTextColor}};
        let [mMarkedDates] = this.setupMarkedDates(day.dateString, toDate, markedDates);
        this.setState({fromDate: day.dateString, isFromDatePicked: true, isToDatesPicked: true, markedDates: mMarkedDates});
        this.props.onSuccess(Object.keys(mMarkedDates));
    };

    setupMarkedDates = (fromDate, toDate, markedDates) => {
        let mFromDate = new XDate(fromDate)
        let mToDate = new XDate(toDate)
        let range = mFromDate.diffDays(mToDate)
        for (var i = 1; i <= range; i++) {
            let tempDate = mFromDate.addDays(1).toString('yyyy-MM-dd');
            if (i < range) {
                markedDates[tempDate] = {color: this.props.theme.markColor, textColor: this.props.theme.markTextColor}
            } else {
                markedDates[tempDate] = {endingDay: true, color: this.props.theme.markColor, textColor: this.props.theme.markTextColor}
            }
        }
        return [markedDates, range]
    };

    _handleDayPress = (day) => {this.onDayPress(day)};

    _renderArrow = (direction) => {
        if (direction === 'left') return <Arrow direction="left"/>;
        return <Arrow direction="right"/>
    };

    setupInitialRange = () => {
        if (!this.props.initialRange) return;
        let [fromDate, toDate] = this.props.initialRange;
        let markedDates = {[fromDate]: {startingDay: true, color: this.props.theme.markColor, textColor: this.props.theme.markTextColor}};
        let [mMarkedDates, range] = this.setupMarkedDates(fromDate, toDate, markedDates);
        this.setState({markedDates: mMarkedDates, fromDate: fromDate});
        this.props.onSuccess(Object.keys(mMarkedDates));
    };

    render() {
        return (
            <Calendar
                {...this.props}
                markingType={'period'}
                current={this.state.fromDate}
                markedDates={this.state.markedDates}
                onDayPress={this._handleDayPress}
                renderArrow={this._renderArrow}
            />
        )
    }
}

DateRangePicker.defaultProps = {
    theme: { markColor: '#00adf5', markTextColor: '#ffffff' }
};