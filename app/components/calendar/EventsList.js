import React from 'react';
import {View, StyleSheet} from "react-native";
import {Text} from "react-native-animatable";
import EventItem from "./EventItem";
import moment from 'moment';
import {capitalize} from "../../helpers/functions";

export default class EventsList extends React.Component {

    shouldComponentUpdate(nextProps: Readonly<P>, nextState: Readonly<S>, nextContext: any): boolean {
        return false;
    }

    render() {
        const {events} = this.props;

        return (
            <View style={styles.main_container}>
                {
                    events.map(item => (
                        <View key={item.id} style={{flex: 1}}>
                            <View>
                                <Text style={[styles.date, {color: item.color}]}>{capitalize(moment(item.date).format("ddd DD MMM"))}</Text>
                                {
                                    item.items.map(itemEvent => (
                                        <EventItem key={itemEvent.id} event={itemEvent}/>
                                    ))
                                }
                            </View>
                        </View>
                    ))
                }
            </View>
        );
    }

}

const styles = StyleSheet.create({
    main_container: {
        flex: 1,
    },
    date: {
        paddingTop: 10,
        paddingBottom: 10,
        paddingLeft: 10,
        fontSize: 16,
        fontWeight: "bold"
    }
});