import React from "react";
import {StyleSheet, Text, View} from "react-native";
import PropTypes from 'prop-types';

export default class LabelItem extends React.Component {

    shouldComponentUpdate(nextProps: Readonly<P>, nextState: Readonly<S>, nextContext: any): boolean {
        return false;
    }

    render() {
        const {label, isFocused} = this.props;
        const backgroundColor = isFocused ? '#fe6577':'white';
        const color = isFocused ? "white":'#4a4b81';

        return (
            <View style={[styles.calendarItem, {backgroundColor}]}>
                <Text style={{color}}>{label}</Text>
            </View>
        );
    }

}

LabelItem.propTypes = {
    label: PropTypes.string.isRequired,
    isFocused: PropTypes.bool,
};

LabelItem.defaultProps = {
    isFocused: false,
};

const styles = StyleSheet.create({
    calendarItem: {
        justifyContent: "center",
        alignItems: "center",
        paddingTop: 5,
        paddingBottom: 5,
        width: 35,
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20
    }
});