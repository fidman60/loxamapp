import React from 'react';
import {View, StyleSheet, Text, Dimensions, Image} from "react-native";
import Icon from "react-native-vector-icons/MaterialIcons";

const {width} = Dimensions.get('window');

export default class EventItem extends React.Component {

    shouldComponentUpdate(nextProps: Readonly<P>, nextState: Readonly<S>, nextContext: any): boolean {
        return false;
    }

    render() {
        const {event} = this.props;
        return (
            <View style={styles.container}>
                <View style={[styles.firstSection, {borderColor: event.itemColor}]}>
                    <Text style={styles.firstText}>{event.firstTime}</Text>
                    <Text style={styles.secondText}>{event.secondTime}</Text>
                </View>
                <View style={styles.secondSection}>
                    <Text style={styles.firstText}>{event.firstTitle}</Text>
                    <Text style={styles.secondText}>{event.secondTitle}</Text>
                </View>
                <View style={{flexDirection: "row", alignItems: "center"}}>
                    <Icon name={event.out ? 'keyboard-arrow-right':'keyboard-arrow-left'} size={15}/>
                    <Image source={require('../../assets/img/machine.jpg')} style={{width: 40, height: 30}}/>
                </View>
            </View>
        );
    }

}

const styles = StyleSheet.create({
    container: {
        padding: 5,
        flexDirection: 'row',
        justifyContent: "space-between",
        alignItems: "center",
        backgroundColor: "white",
        width: width,
        paddingLeft: 15,
        paddingRight: 15,
    },
    firstSection: {
        paddingRight: 10,
        borderRightWidth: 2,
    },
    firstText: {
        color: '#3b3f78',
        fontSize: 15,
    },
    secondSection: {
        paddingLeft: 10,
        flexGrow: 3
    },
    secondText: {
        color: '#e2cdcd',
        fontSize: 15,
    },
});