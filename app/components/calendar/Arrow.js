import React from 'react';
import {View, StyleSheet} from 'react-native';
import Icon from "react-native-vector-icons/MaterialIcons";
import PropTypes from 'prop-types';

export default class Arrow extends React.PureComponent {

    render() {
        const {direction} = this.props;
        return (
            <View style={styles.container}>
                <Icon name={direction === 'left' ? 'keyboard-arrow-left' : 'keyboard-arrow-right'} color='#b1c4cf' size={20}/>
            </View>
        );
    }

}

Arrow.propTypes = {
    direction: PropTypes.oneOf(['left', 'right']).isRequired,
};

const styles = StyleSheet.create({
    container: {
        width: 22,
        height: 22,
        borderRadius: 11,
        backgroundColor: "#e2f1fa",
        alignItems: "center",
        justifyContent: "center",
    }
});