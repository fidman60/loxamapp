import React from 'react';
import {StyleSheet, View} from 'react-native';
import LabelItem from "./LabelItem";
import DayItem from "./DayItem";

export default class DatePickerPlaceHolder extends React.Component {

    shouldComponentUpdate(nextProps: Readonly<P>, nextState: Readonly<S>, nextContext: any): boolean {
        return false;
    }

    render() {
        return (
            <View style={styles.calendarBlockChild}>
                <View style={styles.calendarRow}>
                    <LabelItem label="L"/>
                    <LabelItem label="M"/>
                    <LabelItem label="M"/>
                    <LabelItem label="J"/>
                    <LabelItem label="V"/>
                    <LabelItem label="S" isFocused={true}/>
                    <LabelItem label="D"/>
                </View>
                <View style={styles.calendarRow}>
                    <DayItem day={27} inRange={false}/>
                    <DayItem day={28} inRange={false}/>
                    <DayItem day={29} inRange={false}/>
                    <DayItem day={30} inRange={false}/>
                    <DayItem day={1}/>
                    <DayItem day={2} isFocused={true}/>
                    <DayItem day={3}/>
                </View>
            </View>
        );
    }

}

const styles = StyleSheet.create({
    calendarBlockChild: {
        borderRadius: 5,
        backgroundColor: "white",
        paddingTop: 5,
        paddingBottom: 5,
        paddingLeft: 30,
        paddingRight: 30,
    },
    calendarRow: {
        flexDirection: "row",
        justifyContent: "space-between"
    },
});