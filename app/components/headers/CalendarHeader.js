import React from 'react';
import {Text, TouchableOpacity, View, StyleSheet} from "react-native";
import CustomIcon from "../icons/CustomIcon";
import Icon from "react-native-vector-icons/MaterialIcons";
import LinearGradient from "react-native-linear-gradient";
import PropTypes from 'prop-types';

export default class CalendarHeader extends React.PureComponent {

    render() {
        const {month, handleCalendarToggle} = this.props;

        return (
            <LinearGradient colors={['#fe4861', '#f6253a']} locations={[0.3,1]}>
                <View style={styles.header}>
                    <Text style={styles.headerMonth}>{month}</Text>
                    <View style={styles.headerIcons}>
                        <TouchableOpacity onPress={handleCalendarToggle}>
                            <CustomIcon name="calendar-adjust" size={22} color="white"/>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.headerIcon}>
                            <Icon name="search" size={22} color="white"/>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.headerIcon}>
                            <Icon name="add" size={22} color="white"/>
                        </TouchableOpacity>
                    </View>
                </View>
            </LinearGradient>
        );
    }

}

CalendarHeader.propTypes = {
    month: PropTypes.string.isRequired,
    handleCalendarToggle: PropTypes.func.isRequired,
};

const styles = StyleSheet.create({
    header: {
        flexDirection: "row",
        justifyContent: "space-between",
        paddingTop: 10,
        paddingLeft: 10,
        paddingRight: 10,
        paddingBottom: 50,
    },
    headerMonth: {
        fontSize: 20,
        fontWeight: "bold",
        color: "white"
    },
    headerIcons: {
        flexDirection: "row",
        alignItems: "center",
    },
    headerIcon: {
        marginLeft: 15
    },
});