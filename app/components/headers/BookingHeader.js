import React from 'react';
import {Image, StyleSheet, TouchableOpacity, View} from "react-native";
import {withNavigation} from 'react-navigation';
import CustomIcon from '../icons/CustomIcon';

class BookingHeader extends React.Component {

    _handleMenuToggle = () => {
        this.props.navigation.toggleDrawer();
    };

    shouldComponentUpdate(nextProps: Readonly<P>, nextState: Readonly<S>, nextContext: any): boolean {
        return false;
    }

    render() {
        return (
            <View style={styles.container}>
                <TouchableOpacity onPress={this._handleMenuToggle}>
                    <CustomIcon name="menu" size={20} color="#363636"/>
                </TouchableOpacity>
                <Image source={require('../../assets/img/logo.jpg')} style={styles.logo} resizeMode="contain"/>
                <Image source={require('../../assets/img/user_icon.png')} style={styles.profileImage}/>
            </View>
        );
    }

}

export default withNavigation(BookingHeader);

const styles = StyleSheet.create({
    container: {
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center",
        paddingLeft: 15,
        paddingRight: 15,
        paddingTop: 5,
    },
    logo: {
        width: 150,
    },
    profileImage: {
        width: 30,
        height: 30,
        borderRadius: 15,
    }
});