import React from 'react';
import HorizontalBookingViewList from "../../components/list-view/HorizontalBookingViewList";
import {bookingItems} from "../../service/data";
import BookingLayout from "../../components/layouts/BookingLayout";

export default class BookingScreen extends React.Component {
    render() {
        return (
            <BookingLayout>
                <HorizontalBookingViewList title="Énergie électrique et Fluides" items={bookingItems}/>
                <HorizontalBookingViewList title="Outillage professionnel" items={bookingItems}/>
                <HorizontalBookingViewList title="Terrassement et Routes" items={bookingItems}/>
            </BookingLayout>
        );
    }

}