import React from 'react';
import {StyleSheet, Text, View, TouchableOpacity} from "react-native";
import BookingLayout from "../../components/layouts/BookingLayout";
import ProgressSteps from "../../components/progress-bars/ProgressSteps";
import {calendarStyle, calendarTheme} from "../../config/calendarTheme";
import ManualDateRangePicker from "../../components/calendar/ManualDateRangePicker";
import OrderQuantityItem from "../../components/items/OrderQuantityItem";
import {Button} from "react-native-elements";
import LinearGradient from "react-native-linear-gradient";

export default class OrderScreen extends React.Component {

    constructor(props){
        super(props);

        this.state = {
            dates: [],
            quantity: 1,
        }
    }

    _onSelectRange = (dates) => {
        this.setState({dates});
    };

    _onQuantityPlus = () => {
        this.setState({
            quantity: this.state.quantity+1
        });
    };

    _onQuantityMinus = () => {
        const {quantity} = this.state;
        if (quantity > 1) {
            this.setState({
                quantity: quantity-1
            });
        }
    };

    _handleDeclinePress = () => {
        this.props.navigation.goBack();
    };

    render() {
        const {quantity} = this.state;
        return (
            <BookingLayout>
                <View style={{paddingLeft: 20, paddingRight: 20, marginTop: 30}}>
                    <ProgressSteps stepNo={1}/>
                </View>
                <View style={{flex: 1,padding: 10, marginTop: 20}}>
                    <Text style={styles.firstText}>Renseignez vos dates de location :</Text>
                    <Text style={styles.secondText}>Living in today’s metropolitan world of cellular phones, mobile computers and other high-tech gadgets is not just hectic </Text>
                    <View style={styles.calendar}>
                        <ManualDateRangePicker
                            style={calendarStyle}
                            onSuccess={this._onSelectRange}
                            theme={calendarTheme}
                        />
                    </View>
                    <View>
                        <Text style={styles.firstText}>Les produits à la location</Text>
                        <View style={styles.item}>
                            <OrderQuantityItem
                                quantity={quantity}
                                onMinus={this._onQuantityMinus}
                                onPlus={this._onQuantityPlus}
                                item={this.props.navigation.getParam('item')}
                            />
                        </View>
                    </View>
                    <View style={styles.btnsRow}>
                        <Button
                            title="Annuler"
                            containerStyle={styles.btnContainer}
                            titleStyle={styles.declineBtnTitle}
                            buttonStyle={styles.declineBtn}
                            onPress={this._handleDeclinePress}
                        />
                        <Button
                            containerStyle={styles.btnContainer}
                            buttonStyle={{borderRadius: 100}}
                            title="Continuer"
                            ViewComponent={LinearGradient}
                            linearGradientProps={{
                                colors: ['rgb(253,78,109)', 'rgb(251,38,59)'],
                            }}
                        />
                    </View>
                </View>
            </BookingLayout>
        );
    }

}

const styles = StyleSheet.create({
    firstText: {
        color: "#484c7f",
        fontSize: 17,
        marginBottom: 10
    },
    secondText: {
        color: "#4a4a4a",
        textAlign: "auto"
    },
    calendar: {
        marginTop: 20,
        marginBottom: 23,
        shadowColor: "white",
        shadowOffset: {
            width: 10,
            height: 10,
        },
        shadowOpacity: 0.1,
        shadowRadius: 1.65,
        elevation: 8,
    },
    btnsRow: {
        flexDirection: "row",
        marginTop: 30
    },
    btnContainer: {
        flex: 1,
    },
    declineBtnTitle: {
        color: "#b1b7ca"
    },
    declineBtn: {
        backgroundColor: "transparent"
    },
    item: {
        marginTop: 15
    }
});