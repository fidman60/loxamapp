const events = [
    {
        id: 1,
        date: '2019-10-4',
        color: '#fc3753',
        items: [
            {
                id: 1,
                itemColor: '#fc243d',
                firstTime: '17:00',
                secondTime: '',
                firstTitle: 'Groupe électrogène',
                secondTitle: 'V7 Digital Printing',
                out: true,
                //image: url
            }
        ]
    },
    {
        id: 2,
        date: '2019-10-9',
        color: '#494c7f',
        items: [
            {
                id: 1,
                itemColor: '#a9caf9',
                firstTime: 'Fin',
                secondTime: '17:00',
                firstTitle: 'Groupe électrogène',
                secondTitle: 'V7 Digital Printing',
                out: true,
            }
        ]
    },
    {
        id: 3,
        date: '2019-10-20',
        color: '#494c7f',
        items: [
            {
                id: 1,
                itemColor: '#fc243d',
                firstTime: '15:00',
                secondTime: '17:00',
                firstTitle: 'Effective Forms Advertising',
                secondTitle: 'Mauritania',
                out: false,
            },
            {
                id: 2,
                itemColor: '#a9caf9',
                firstTime: 'Fin',
                secondTime: '17:00',
                firstTitle: 'Winner S Gold',
                secondTitle: 'Afghanistan',
                out: true,
            },
            {
                id: 3,
                itemColor: '#a9caf9',
                firstTime: '22:00',
                secondTime: '23:44',
                firstTitle: 'Business And Other',
                secondTitle: 'Tajikistan',
                out: false,
            },
        ]
    },
    {
        id: 4,
        date: '2019-10-29',
        color: '#494c7f',
        items: [
            {
                id: 1,
                itemColor: '#fc243d',
                firstTime: '17:00',
                secondTime: '',
                firstTitle: 'Groupe électrogène',
                secondTitle: 'V7 Digital Printing',
                out: true,
            },
        ]
    },
    {
        id: 5,
        date: '2019-10-12',
        color: '#494c7f',
        items: [
            {
                id: 1,
                itemColor: '#a9caf9',
                firstTime: 'Fin',
                secondTime: '17:00',
                firstTitle: 'Rdv Eurostad',
                secondTitle: 'V7 Digital Printing',
                out: true,
            },
        ]
    },
    {
        id: 6,
        date: '2019-10-16',
        color: '#494c7f',
        items: [
            {
                id: 1,
                itemColor: '#fc243d',
                firstTime: '15:00',
                secondTime: '17:00',
                firstTitle: 'Effective Forms Advertising',
                secondTitle: 'Mauritania',
                out: false,
            },
        ]
    },
];


const bookingItems = [
    {
        id: 1,
        title: "Groupe électrogène",
        price: 56.43,
        isReserved: false,
        inBookmark: true,
        description: "Automotrice - 5 cm",
        ref: '012-1214'
    },
    {
        id: 2,
        title: "Compresseur",
        price: 85.43,
        isReserved: true,
        inBookmark: true,
        description: "Automotrice - 5 cm",
        ref: '002-2333'
    },
    {
        id: 3,
        title: "Groupe électrogène",
        price: 60.99,
        isReserved: false,
        inBookmark: false,
        description: "Automotrice - 20 cm",
        ref: '012-1214'
    },
    {
        id: 4,
        title: "Traitement de l'air",
        price: 40.99,
        isReserved: false,
        inBookmark: false,
        description: "Automotrice - 30 cm",
        ref: '022-2391'
    },
    {
        id: 5,
        title: "Traitement de l'air",
        price: 40.99,
        isReserved: false,
        inBookmark: true,
        description: "Automotrice - 34 cm",
        ref: '123-8402'
    },
];

export {events, bookingItems};