const CALENDAR_HEIGHT = 320;

const calendarTheme = {
    dayTextColor: '#58627d',
    markColor: '#fe8190',
    markTextColor: 'white',
    'stylesheet.calendar.header': {
        header: {
            flexDirection: 'row',
            justifyContent: 'space-between',
            paddingLeft: 10,
            paddingRight: 10,
            marginTop: 6,
            alignItems: 'center',
            marginBottom: 15
        },
        arrow: {
            padding: 0
        },
        monthText: {
            fontWeight: "bold",
            color: "#484c7f"
        },
        dayHeader: {
            color: "#97a2ac",
        },
        week: {
            marginTop: 7,
            flexDirection: 'row',
            justifyContent: 'space-around',
            marginBottom: 10
        }
    },
    'stylesheet.day.period': {
        base: {
            borderTopLeftRadius: 30,
            borderBottomLeftRadius: 30,
            borderTopRightRadius: 30,
            borderBottomRightRadius: 30,
            width: 38,
            height: 34,
            alignItems: 'center'
        },
    },
    'stylesheet.calendar.main': {
        week: {
            marginTop: 2,
            marginBottom: 2,
            flexDirection: 'row',
            justifyContent: 'space-around'
        },
    }
};

const calendarStyle = {
    height: CALENDAR_HEIGHT,
    borderRadius: 4,
    paddingLeft: 20,
    paddingRight: 20,
    paddingTop: 10,
};

export {calendarTheme, CALENDAR_HEIGHT, calendarStyle};