import React from 'react';
import { createAppContainer, createSwitchNavigator } from 'react-navigation';
import { createBottomTabNavigator, createMaterialTopTabNavigator } from 'react-navigation-tabs';
import { Transition } from 'react-native-reanimated';
import createAnimatedSwitchNavigator from 'react-navigation-animated-switch';
import {createDrawerNavigator} from 'react-navigation-drawer';
import CalendarScreen from "./app/screens/CalendarScreen";
import NotificationsScreen from "./app/screens/NotificationsScreen";
import ProfileScreen from "./app/screens/ProfileScreen";
import MaterialIcons from "react-native-vector-icons/MaterialIcons"
import IconWithBadge from "./app/components/icons/IconWithBadge";
import CustomIcon from "./app/components/icons/CustomIcon";
import BookingScreen from "./app/screens/booking/BookingScreen";
import {ActivityIndicator, View} from "react-native";
import SplashScreen from "./app/screens/SplashScreen";
import OrderScreen from "./app/screens/booking/OrderScreen";
import {createStackNavigator} from "react-navigation-stack";

const TAB_ICON_SIZE = 20;
/*
const BottomTabNavigator = createBottomTabNavigator({
    Today: {
        screen: Today,
        navigationOptions: {
            tabBarLabel: "Aujourd'hui"
        }
    },
    Booking: {
        screen: Booking,
        navigationOptions: {
            tabBarLabel: "Réserver",
        }
    },
    Calendar: {
        screen: Calendar,
        navigationOptions: {
            tabBarLabel: "Calendrier"
        }
    },
    Notifications: {
        screen: Notifications,
        navigationOptions: {
            tabBarLabel: "Notifications"
        }
    },
}, {
    lazy: true,
    defaultNavigationOptions: ({ navigation }) => ({
        tabBarIcon: ({ focused, horizontal, tintColor }) => {
            const { routeName } = navigation.state;
            let IconComponent = MaterialIcons;
            let iconName;
            if (routeName === 'Today') {
                iconName = 'event-available';
            } else if (routeName === 'Calendar') {
                iconName = 'event-note';
            } else if (routeName === 'Booking') {
                iconName = 'local-offer';
            } else {
                iconName = 'notifications-none';
                return (
                    <IconWithBadge
                        name={iconName}
                        badgeCount={3}
                        color={tintColor}
                        size={TAB_ICON_SIZE}
                        badgeColor={focused ? '#c61d32':'#494c7f'}
                    />
                )
            }
            // You can return any component that you like here!
            return <IconComponent name={iconName} size={TAB_ICON_SIZE} color={tintColor} />;
        },
    }),
    tabBarOptions: {
        activeTintColor: '#fb5065',
        inactiveTintColor: '#a9abc2',
        style: {
            height: 60,
            padding: 7,
            borderTopWidth: 0,
        },
    },
});
*/

const BookingStackNavigator = createStackNavigator({
    BookingScreen: {
        screen: BookingScreen,
    },
    OrderScreen: {
        screen: OrderScreen,
    },
},{
    initialRouteName: 'BookingScreen',
    headerMode: "none",
});


const BottomTabNavigator = createMaterialTopTabNavigator({
    Profile: {
        screen: ProfileScreen,
        navigationOptions: {
            tabBarLabel: "Profile",
        }
    },
    BookingStack: {
        screen: BookingStackNavigator,
        navigationOptions: {
            tabBarLabel: "Réserver",
            swipeEnabled: false,
        }
    },
    Calendar: {
        screen: CalendarScreen,
        navigationOptions: {
            tabBarLabel: "Calendrier"
        }
    },
    Notifications: {
        screen: NotificationsScreen,
        navigationOptions: {
            tabBarLabel: "Réglages"
        }
    },
}, {
    lazy: true,
    lazyPlaceholderComponent: () => (<View style={{marginTop: 10}}><ActivityIndicator size={30} color="red" /></View>),
    tabBarPosition: 'bottom',
    backBehavior: "history",
    defaultNavigationOptions: ({ navigation }) => ({
        tabBarIcon: ({ focused, horizontal, tintColor }) => {
            const { routeName } = navigation.state;
            let iconName;
            if (routeName === 'Profile') {
                iconName = 'profile';
            } else if (routeName === 'Calendar') {
                iconName = 'calendar';
            } else if (routeName === 'BookingStack') {
                iconName = 'booking';
            } else {
                iconName = 'settings';
            }
            // You can return any component that you like here!
            return <CustomIcon name={iconName} size={23} color={tintColor} />;
        },
    }),
    tabBarOptions: {
        activeTintColor: '#fb5065',
        inactiveTintColor: '#a9abc2',
        upperCaseLabel: false,
        showIcon: true,
        style: {
            backgroundColor: "transparent",
        },
        tabStyle: {
            padding: 0,
            paddingTop: 10,
            paddingBottom: 10,
        },
        contentContainerStyle: {
            paddingRight: 20,
            paddingLeft: 20
        },
        iconStyle: {
            width: 23,
            height: 23,
            margin: 0
        },
        labelStyle: {
            margin: 0,
            marginBottom: 2,
        },
        indicatorStyle: {
            /*backgroundColor: "black",
            height: 5,
            borderRadius: 5*/
            height: 0,
        }
    },
});

const DrawerNavigator = createDrawerNavigator({
    MainTabNav: {
        screen: BottomTabNavigator,
        navigationOptions: {
            drawerLabel: "Home"
        }
    },
});

const switchNavigator = createAnimatedSwitchNavigator({
    App: DrawerNavigator,
    Splash: SplashScreen,
}, {
    initialRouteName: "Splash",
    transition: (
        <Transition.Together>
            <Transition.Out
                type="slide-bottom"
                durationMs={400}
                interpolation="easeIn"
            />
            <Transition.In type="fade" durationMs={300} />
        </Transition.Together>
    ),
});


export default createAppContainer(switchNavigator);