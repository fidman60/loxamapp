/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React from 'react';
import Root from './Root';
import * as moment from 'moment';
import 'moment/locale/fr';
import {LocaleConfig} from "react-native-calendars";

moment.locale("fr");

LocaleConfig.locales.en = LocaleConfig.locales[''];

LocaleConfig.locales['fr'] = {
  monthNames: ['Janvier','Février','Mars','Avril','Mai','Juin','Juillet','Août','Septembre','Octobre','Novembre','Décembre'],
  monthNamesShort: ['Janv.','Févr.','Mars','Avril','Mai','Juin','Juil.','Août','Sept.','Oct.','Nov.','Déc.'],
  dayNames: ['Dimanche','Lundi','Mardi','Mercredi','Jeudi','Vendredi','Samedi'],
  dayNamesShort: ['D.','L.','M.','M.','J.','V.','S.'],
  today: 'Aujourd\'hui'
};

LocaleConfig.defaultLocale = 'fr';

const App: () => React$Node = () => {
  return (
    <Root/>
  );
};

export default App;
